//
//  ViewController.swift
//  AnimationApp
//
//  Created by Maksim on 28.02.2022.
//

// import UIKit не обязателен, тк Spring уже его содержит.

import Spring

class ViewController: UIViewController {

    @IBOutlet var coreAnimationView: UIView!
    @IBOutlet var springAnimationView: SpringView!
    
    private var originCoordinate: CGFloat?
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        // Зададим координату Х
        originCoordinate = coreAnimationView.frame.origin.x
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }

    @IBAction func coreAnimationButtonPressed(_ sender: UIButton) {
        sender.pulsate()
        
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       options: [.autoreverse, .repeat]) { [unowned self] in
            if self.coreAnimationView.frame.origin.x == self.originCoordinate {
                self.coreAnimationView.frame.origin.x -= 20
            }
        }
   
    }
    @IBAction func springAnimationButtonPressed(_ sender: SpringButton) {
        springAnimationView.animation = Spring.AnimationPreset.fadeInUp.rawValue
        springAnimationView.curve = Spring.AnimationCurve.easeInBack.rawValue
        springAnimationView.duration = 1
        springAnimationView.animate()
        
        
        sender.animation = Spring.AnimationPreset.shake.rawValue
        sender.animate()
    }
    
}

